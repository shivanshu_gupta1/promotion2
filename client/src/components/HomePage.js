import React, {useState} from 'react';
import axios from 'axios';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import { auth } from '../Firebase/index';
// URI
import { API_SERVICE } from '../config/URI';
// stripe
import {useStripe, useElements, CardElement} from '@stripe/react-stripe-js';
// Util imports
import {makeStyles} from '@material-ui/core/styles';
// Custom Components
import CardInput from './CardInput';
import { firestore } from "../Firebase/index";

const useStyles = makeStyles({
  root: {
    maxWidth: 600,
    marginTop: '4px'
  },
  content: {
    display: 'flex',
    flexDirection: 'column',
    alignContent: 'flex-start',
  },
  div: {
    display: 'flex',
    flexDirection: 'row',
    alignContent: 'flex-start',
    justifyContent: 'space-between',
  },
  button: {
    margin: '2em auto 1em',
  },
});

function HomePage({ firstname, lastname, address, zipcode, email, businessphoneno, type, amount }) {
  const classes = useStyles();
  // State
  const [paysubmit, setPaySubmit] = useState(false);

  const stripe = useStripe();
  const elements = useElements();


  const handleSubmit = async (event) => {
    setPaySubmit(true);
    // var e = sessionStorage.getItem("email");
    if (!stripe || !elements) {
      // Stripe.js has not yet loaded.
      // Make sure to disable form submission until Stripe.js has loaded.
      return;
    }

    const res = await axios.post(`${API_SERVICE}/api/v1/main/charges`, {email: email, amount: amount});

    const clientSecret = res.data['client_secret'];

    const result = await stripe.confirmCardPayment(clientSecret, {
      payment_method: {
        card: elements.getElement(CardElement),
        billing_details: {
          email: email,
        },
      },
    });

    if (result.error) {
      // Show error to your customer (e.g., insufficient funds)
      console.log(result.error.message);
    } else {
      // The payment has been processed!
      if (result.paymentIntent.status === 'succeeded') {
        var uploadData = {
          transactionId: result.paymentIntent.id,
          email,
          firstname,
          lastname,
          businessphoneno,
          address,
          zipcode,
          amount
        }
        axios.post(`${API_SERVICE}/api/v1/main/paymentsuccessfull`, uploadData)
            .then((response) => {
                if (response.status === 200) {
                    window.location.href = `/thankyou`;
                } 
            }).catch(err => console.log(err));
        // Show a success message to your customer
        // There's a risk of the customer closing the window before callback
        // execution. Set up a webhook or plugin to listen for the
        // payment_intent.succeeded event that handles any business critical
        // post-payment actions.
      }
    }
  };

  return (
    <center>
      <Card className={classes.root}>
          <h2 className="text-center font-weight-bold">
          Please Pay {amount} USD to Continue 
          </h2>
        <CardContent className={classes.content}>
          <input 
              className="form-control mb-2"
              label='Full Name'
              id='outlined-email-input'
              helperText={`Full Name`}
              margin='normal'
              type='text'
              placeholder="Full Name"
              required
              fullWidth
              value={`${firstname} ${lastname}`}
          />
          <input 
              className="form-control mb-2"
              label='Address'
              id='outlined-email-input'
              helperText={`Address`}
              margin='normal'
              type='text'
              placeholder="Address"
              required
              fullWidth
              value={address}
          />
          <input 
              className="form-control mb-2"
              label='ZIP Code'
              id='outlined-email-input'
              helperText={`Zip Code 201001`}
              margin='normal'
              type='text'
              placeholder="ZIP Code"
              required
              fullWidth
              value={zipcode}
          />
          <input 
              className="form-control mb-2"
              label='Phone No.'
              id='outlined-email-input'
              helperText={`Phone No to Contact You`}
              margin='normal'
              type='text'
              required
              fullWidth
              placeholder="+1 "
              value={businessphoneno}
          />
          <input 
              className="form-control mb-2"
              label='Email'
              id='outlined-email-input'
              helperText={`Email you'll recive updates and receipts on`}
              margin='normal'
              type='email'
              required
              fullWidth
              value={email}
          />
          <br />
          <CardInput />
          <div className={classes.div}>
            <button disabled={paysubmit} onClick={handleSubmit} style={{ fontSize: '2vh' }} className="btn btn-lg w-100 mt-5 btn-primary">
              {
                paysubmit ? (
                  <>Please Be Paisence ...</>
                ) : (
                  <>
                  Pay
                  </>
                )
              }
            </button>
          </div>
        </CardContent>
      </Card>
    </center>
  );
}

export default HomePage;