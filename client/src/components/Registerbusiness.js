import React from 'react';
import { API_SERVICE } from '../config/URI';
import axios from 'axios';
import queryString from 'query-string';
import { auth } from "../Firebase/index";

// Components
import Header from './static/Header';
import Footer from './static/Footer';

// Stripe
import {Elements} from '@stripe/react-stripe-js';
import {loadStripe} from '@stripe/stripe-js';
import HomePage from './HomePage';
// const stripePromise = loadStripe('pk_test_51IQmDSLbp71n4XnIqbqqCefhzj2vC0QfGa0CXHMcuF0XkfgBu5O8G8C3ArCAhTamuUISvENitKXwmCYB9CGdbTHo008s70cMF8');
const stripePromise = loadStripe('pk_test_51IWmCjIMEJNIatcZxjBvtC1UE2QUKie1MSrEHZI2NW6dpYBPvtotiAPdODHXCGxfokNQsTtuze8jtDo0QBBQuXDl00b2UhlrPH');

const Registerbusiness = ({ location }) => {
    const [step, setstep] = React.useState(1);
    const [businessname, setbusinessname] = React.useState('');
    const [city, setcity] = React.useState('');
    const [state, setstate] = React.useState('');
    const [zipcode, setzipcode] = React.useState('');
    const [businesscategory, setbusinesscategory] = React.useState('');
    const [businessphoneno, setbusinessphoneno] = React.useState('');
    const [website, setwebsite] = React.useState('');
    const [address, setaddress] = React.useState('');
    const [email, setemail] = React.useState('');
    const [firstname, setfirstname] = React.useState('');
    const [lastname, setlastname] = React.useState('');
    const [password, setpassword] = React.useState('');
    const [businessId, setbusinessId] = React.useState('');

    const signUp = () => {
        auth.createUserWithEmailAndPassword(email, password)
        .then((result) => {
            var user = result.user;
            user.updateProfile({
                photoURL: "https://kittyinpink.co.uk/wp-content/uploads/2016/12/facebook-default-photo-male_1-1.jpg",
                displayName: `${firstname} ${lastname}`
            })
            .then(() => {
              user.sendEmailVerification().then(function() {
                console.log("Verification Send");
              }).catch(function(error) {
                  console.log(error);
              });
            })
            .catch(err => console.log(err))
        })
        .catch(function(error) {
            console.log(error);
        });
    }

    const submitDetails = (plan) => {
        signUp();
        if (plan === "free") {
            var uploadData = {
                city,
                state,
                zipcode,
                businesscategory,
                businessphoneno,
                website,
                address,
                email,
                firstname,
                lastname,
                password,
                businessId,
                plan,
                paid: false
            }
            axios.post(`${API_SERVICE}/api/v1/main/registerbusiness2`, uploadData)
                .then((d) => {
                    setstep(5);
                }).catch(err => console.log(err));
        } else {
            var uploadData = {
                city,
                state,
                zipcode,
                businesscategory,
                businessphoneno,
                website,
                address,
                email,
                firstname,
                lastname,
                password,
                businessId,
                plan,
                paid: false
            }
            axios.post(`${API_SERVICE}/api/v1/main/registerbusiness2`, uploadData)
                .then((d) => {
                    setstep(4);
                }).catch(err => console.log(err));
        }
        
    }

    React.useEffect(() => {
        const { q } = queryString.parse(location.search);
        setbusinessId(q);
        axios.get(`${API_SERVICE}/api/v1/main/fetchbusinessdetails/${q}`)
            .then((d) => {
                setbusinessname(d.data[0].businessname);
            }).catch(err => console.log(err));
    }, []);

    return (
        <>
            <main className="page-wrapper">
                <Header />
                    {
                        step === 1 ? (
                            <>
                            <h2 class="mb-5 mt-5 text-center">Register your Business</h2>
                            <div className="mt-5 mb-5 container w-50">
                                <div class="form-floating mb-3">
                                <input value={businessname} disabled class="form-control" type="text" id="fl-text" placeholder="Business Name" />
                                <label for="fl-text">Business Name</label>
                                </div>

                                <div className="row">
                                    <div className="col">
                                        <div class="form-floating mb-3">
                                        <input class="form-control" onChange={(e) => setcity(e.target.value)} type="text" id="fl-text" placeholder="City" />
                                        <label for="fl-text">City</label>
                                        </div>
                                    </div>
                                    <div className="col">
                                        <div class="form-floating mb-3">
                                        <input class="form-control" onChange={(e) => setstate(e.target.value)} type="text" id="fl-text" placeholder="State" />
                                        <label for="fl-text">State</label>
                                        </div>
                                    </div>
                                </div>    

                                <div class="form-floating mb-3">
                                <input class="form-control" type="text" onChange={(e) => setzipcode(e.target.value)} id="fl-text" placeholder="ZIP Code" />
                                <label for="fl-text">ZIP Code</label>
                                </div>
                                <div class="form-floating mb-3">
                                <input class="form-control" type="text" onChange={(e) => setbusinesscategory(e.target.value)} id="fl-text" placeholder="Business Category" />
                                <label for="fl-text">Business Category</label>
                                </div>
                                <div class="form-floating mb-3">
                                <input class="form-control" type="text" onChange={(e) => setbusinessphoneno(e.target.value)} id="fl-text" placeholder="Business Phone No" />
                                <label for="fl-text">Business Phone No</label>
                                </div>

                                <div className="row">
                                    <div className="col">
                                        <div class="form-floating mb-3">
                                        <input class="form-control" type="text" onChange={(e) => setwebsite(e.target.value)} id="fl-text" placeholder="Website" />
                                        <label for="fl-text">Website</label>
                                        </div>
                                    </div>
                                    <div className="col">
                                        <div class="form-floating mb-3">
                                        <input class="form-control" type="text" onChange={(e) => setaddress(e.target.value)} id="fl-text" placeholder="Street Address" />
                                        <label for="fl-text">Street Address</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-floating mb-3">
                                <input class="form-control" type="email" id="fl-text" onChange={(e) => setemail(e.target.value)} placeholder="Your Email Address" />
                                <label for="fl-text">Your Email Address</label>
                                </div>
                                <div class="form-floating mb-3">
                                <p>
                                By continuing, you agree to Yelp’s Business Terms and acknowledge our Privacy Policy. We may email you about Yelp’s products, services and local events. You can unsubscribe at any time.
                                </p>
                                </div>
                                <button type="button" onClick={() => setstep(2)} class="btn w-100 btn-primary mt-2 mb-2">Register Business</button>
                            </div>
                            </>
                        ) : step === 2 ? (
                            <>
                            <h2 class="mb-5 mt-5 text-center text-dark">Great! Now create your business account.</h2>
                            <h5 className="text-dark mt-5 mb-5 container w-50">
                            A business account enables you to manage your page, upload photos, and respond to reviews on Neighborhoodeals.
                            </h5>
                            <div className="mt-5 mb-5 container w-50">
                                <div className="row">
                                    <div className="col-md">
                                    <div class="form-floating mb-3">
                                        <input class="form-control" type="text" onChange={(e) => setfirstname(e.target.value)} id="fl-text" placeholder="First Name" />
                                        <label for="fl-text">First Name</label>
                                    </div>
                                    </div>
                                    <div className="col-md">
                                    <div class="form-floating mb-3">
                                        <input class="form-control" type="text" id="fl-text" onChange={(e) => setlastname(e.target.value)} placeholder="Last Name" />
                                        <label for="fl-text">Last Name</label>
                                    </div>
                                    </div>
                                </div>
                                <div class="form-floating mb-3">
                                    <input value={email} disabled class="form-control" type="text" id="fl-text" placeholder="Email" />
                                    <label for="fl-text">Email</label>
                                </div>
                                <div class="form-floating mb-3">
                                    <input class="form-control" type="password" id="fl-text" onChange={(e) => setpassword(e.target.value)} placeholder="Password" />
                                    <label for="fl-text">Password</label>
                                </div>
                                <button type="button" onClick={() => setstep(3)} class="btn w-100 btn-primary mt-2 mb-2">Continue</button>
                            </div>
                            </>
                        ) : step === 3 ? (
                            <>
                            <div className="container mt-5 w-75 mb-5">
                                <h2 class="mb-5 mt-5 text-center text-dark">Please Select Your Subscription</h2>
                                <div className="row">
                                    <div className="col-md">
                                        <div class="card w-100">
                                            <div class="card-img-top bg-secondary text-center py-5 px-grid-gutter">
                                                <h3 class="text-body mb-0">Free</h3>
                                            </div>
                                            <div class="card-body px-grid-gutter py-grid-gutter">
                                                <div class="d-flex align-items-end py-2 px-4 mb-4">
                                                <span class="h2 fw-normal text-muted mb-1 me-2">$</span>
                                                <span class="price display-2 fw-normal text-primary px-1 me-2">0</span>
                                                <span class="h3 fs-lg fw-medium text-muted mb-2">per<br />month</span>
                                                </div>
                                                <ul class="list-unstyled py-2 mb-4">
                                                <li class="d-flex align-items-center mb-3">
                                                    <i class="ai-check fs-xl text-primary me-2"></i>
                                                    <span>20 millions tracks</span>
                                                </li>
                                                <li class="d-flex align-items-center mb-3">
                                                    <i class="ai-check fs-xl text-primary me-2"></i>
                                                    <span>Shuffle play</span>
                                                </li>
                                                <li class="d-flex align-items-center mb-3">
                                                    <i class="ai-x fs-xl text-muted me-2"></i>
                                                    <span class="text-muted">No ads</span>
                                                </li>
                                                <li class="d-flex align-items-center mb-3">
                                                    <i class="ai-x fs-xl text-muted me-2"></i>
                                                    <span class="text-muted">Get unlimited skips</span>
                                                </li>
                                                <li class="d-flex align-items-center mb-3">
                                                    <i class="ai-x fs-xl text-muted me-2"></i>
                                                    <span class="text-muted">Offline mode</span>
                                                </li>
                                                <li class="d-flex align-items-center mb-3">
                                                    <i class="ai-x fs-xl text-muted me-2"></i>
                                                    <span class="text-muted">7 profiles</span>
                                                </li>
                                                </ul>
                                                <div class="text-center mb-2">
                                                <a onClick={() => submitDetails('free')} class="btn btn-outline-primary" href="#">Select Plan</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md">
                                        <div class="card w-100">
                                            <div class="card-img-top bg-secondary text-center py-5 px-grid-gutter">
                                                <h3 class="text-body mb-0">Advanced</h3>
                                            </div>
                                            <div class="card-body px-grid-gutter py-grid-gutter">
                                                <div class="d-flex align-items-end py-2 px-4 mb-4">
                                                <span class="h2 fw-normal text-muted mb-1 me-2">$</span>
                                                <span class="price display-2 fw-normal text-primary px-1 me-2">249</span>
                                                <span class="h3 fs-lg fw-medium text-muted mb-2">per<br />month</span>
                                                </div>
                                                <ul class="list-unstyled py-2 mb-4">
                                                <li class="d-flex align-items-center mb-3">
                                                    <i class="ai-check fs-xl text-primary me-2"></i>
                                                    <span>20 millions tracks</span>
                                                </li>
                                                <li class="d-flex align-items-center mb-3">
                                                    <i class="ai-check fs-xl text-primary me-2"></i>
                                                    <span>Shuffle play</span>
                                                </li>
                                                <li class="d-flex align-items-center mb-3">
                                                    <i class="ai-x fs-xl text-muted me-2"></i>
                                                    <span class="text-muted">No ads</span>
                                                </li>
                                                <li class="d-flex align-items-center mb-3">
                                                    <i class="ai-x fs-xl text-muted me-2"></i>
                                                    <span class="text-muted">Get unlimited skips</span>
                                                </li>
                                                <li class="d-flex align-items-center mb-3">
                                                    <i class="ai-x fs-xl text-muted me-2"></i>
                                                    <span class="text-muted">Offline mode</span>
                                                </li>
                                                <li class="d-flex align-items-center mb-3">
                                                    <i class="ai-x fs-xl text-muted me-2"></i>
                                                    <span class="text-muted">7 profiles</span>
                                                </li>
                                                </ul>
                                                <div class="text-center mb-2">
                                                <a onClick={() => submitDetails('advanced')} class="btn btn-outline-primary" href="#">Select Plan</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </>
                        ) : step === 4 ? (
                            <>
                            <div className="container mt-5 w-75 mb-5">
                            <Elements stripe={stripePromise}>
                                <HomePage firstname={firstname} lastname={lastname} address={address} zipcode={zipcode} email={email} businessphoneno={businessphoneno} type={'vg'} amount={250} />
                            </Elements>
                            </div>
                            </>
                        ) : step === 5 ? (
                            <>
                            <div className="container mt-5 w-75 mb-5 text-center">
                            <center>
                                <img alt="Thankyou" src="https://img.icons8.com/fluent/96/000000/double-tick.png"/>
                            </center>
                            <h2>Thank You for Registering</h2>
                            <h2 className="mb-5">Your account is Successfully Created and is under review.</h2>
                            <br />
                            </div>
                            </>
                        ) : null
                    }
                    
                <Footer />
            </main>
        </>
    )
}

export default Registerbusiness
