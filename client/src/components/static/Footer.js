import React from 'react';

const Footer = () => {
    return (
        <>
            <footer className="footer container">
            <div className="row">
                <div className="col-xl-6 col-lg-5 col-md-4 mb-5 text-center text-md-start">
                <h2 className="mb-4">Talk to us</h2><a className="btn btn-danger mb-4" href="#"><i className="ai-message-square fs-lg me-2"></i>Chat now</a>
                <div className="pt-2"><a className="btn-social bs-outline bs-lg bs-facebook me-2 mb-2" href="#"><i className="ai-facebook"></i></a><a className="btn-social bs-outline bs-lg bs-twitter me-2 mb-2" href="#"><i className="ai-twitter"></i></a><a className="btn-social bs-outline bs-lg bs-instagram me-2 mb-2" href="#"><i className="ai-instagram"></i></a><a className="btn-social bs-outline bs-lg bs-google me-2 mb-2" href="#"><i className="ai-google"></i></a></div>
                </div>
                <div className="col-xl-6 col-lg-7 col-md-8 mb-5">
                <div className="row">
                    <div className="col-6 col-sm-4">
                    <div className="widget">
                        <h4 className="widget-title">Company</h4>
                        <ul>
                        <li><a className="widget-link" href="#">About</a></li>
                        <li><a className="widget-link" href="#">Jobs</a></li>
                        <li><a className="widget-link" href="#">Contacts</a></li>
                        </ul>
                    </div>
                    </div>
                    <div className="col-6 col-sm-4">
                    <div className="widget">
                        <h4 className="widget-title">Services</h4>
                        <ul>
                        <li><a className="widget-link" href="#">Strategy</a></li>
                        <li><a className="widget-link" href="#">HR &amp; Talent</a></li>
                        <li><a className="widget-link" href="#">Training</a></li>
                        <li><a className="widget-link" href="#">Analytics</a></li>
                        <li><a className="widget-link" href="#">Sales &amp; Marketing</a></li>
                        </ul>
                    </div>
                    </div>
                    <div className="d-none d-sm-block col-sm-4">
                    <div className="widget">
                        <h4 className="widget-title">Our news</h4>
                        <ul>
                        <li><a className="widget-link" href="#">Latest news</a></li>
                        <li><a className="widget-link" href="#">Insights</a></li>
                        <li><a className="widget-link" href="#">Campaigns</a></li>
                        <li><a className="widget-link" href="#">Events</a></li>
                        </ul>
                    </div>
                    </div>
                </div>
                </div>
            </div>
            <div className="border-top py-4">
                <div className="d-md-flex align-items-center justify-content-between py-2 text-center text-md-start">
                <ul className="list-inline fs-sm mb-3 mb-md-0 order-md-2">
                    <li className="list-inline-item my-1"><a className="nav-link-style" href="#">Support</a></li>
                    <li className="list-inline-item my-1"><a className="nav-link-style" href="#">Contacts</a></li>
                    <li className="list-inline-item my-1"><a className="nav-link-style" href="#">Terms &amp; Conditions</a></li>
                </ul>
                <p className="fs-sm mb-0 me-3 order-md-1"><span className="text-muted me-1">© All rights reserved | Company Name</span></p>
                </div>
            </div>
            </footer>            
        </>
    )
}

export default Footer
