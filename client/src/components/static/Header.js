import React from 'react'

const Header = () => {
    return (
        <>
            <header className="header navbar navbar-expand-lg navbar-light bg-light navbar-sticky" data-scroll-header="" data-fixed-element="">
                <div className="container px-0 px-xl-3">
                <button className="navbar-toggler ms-n2 me-2" type="button" data-bs-toggle="offcanvas" data-bs-target="#primaryMenu"><span className="navbar-toggler-icon"></span></button><a className="navbar-brand flex-shrink-0 order-lg-1 mx-auto ms-lg-0 pe-lg-2 me-lg-4" href="/">
                    <img src="https://res.cloudinary.com/dx9dnqzaj/image/upload/v1617629479/Big%20SaaS/neighborhoodeals-for-local-business-new_g3xfzu.png" className="w-100 " />
                </a>
                
                <div className="offcanvas offcanvas-collapse order-lg-2" id="primaryMenu">
                <div className="offcanvas-body">
                    <ul className="navbar-nav">
                    <li className="nav-item dropdown dropdown-mega"><a className="nav-link" href="/">Home</a></li>
                    <li className="nav-item dropdown dropdown-mega"><a className="nav-link" href="#">About</a></li>
                    <li className="nav-item dropdown dropdown-mega"><a className="nav-link" href="#">Contact</a></li>
                    </ul>
                </div>
                </div>

                <div className="offcanvas offcanvas-collapse order-lg-2" id="primaryMenu">
                    <div className="offcanvas-cap navbar-shadow">
                    <h5 className="mt-1 mb-0">Menu</h5>
                    <button className="btn-close lead" type="button" data-bs-dismiss="offcanvas" aria-label="Close"></button>
                    </div>
                </div>
                </div>
            </header>
        </>
    )
}

export default Header
