import React from 'react';

// Components
import Header from './static/Header';
import Footer from './static/Footer';

import { API_SERVICE } from '../config/URI';


const ThankYou = () => {
    

    return (
        <>
            <main className="page-wrapper">
                <Header />
                <div className="container mt-5 w-75 mb-5 text-center">
                <center>
                    <img alt="Thankyou" src="https://img.icons8.com/fluent/96/000000/double-tick.png"/>
                </center>
                <h2>Thank You for Registering</h2>
                <h2 className="mb-5">Your account is Successfully Created and is under review.</h2>
                <br />
                </div>
            </main>
            <Footer />
        </>
    )
}

export default ThankYou
