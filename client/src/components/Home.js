import React from 'react';
import axios from 'axios';

// Components
import Header from './static/Header';
import Footer from './static/Footer';

import { API_SERVICE } from '../config/URI';


const Home = () => {
    const [businessname, setbusinessname] = React.useState('');

    const registerbusiness = () => {
        var uploadData = {
            businessname
        }
        axios.post(`${API_SERVICE}/api/v1/main/registerbusiness1`, uploadData)
            .then((d) => {
                window.location.href = `/register?q=${d.data._id}`;
            }).catch(err => console.log(err));
    }

    return (
        <>
        <main className="page-wrapper">
            <Header />
            <section className="container-fluid px-0 pb-5 pb-md-6">
                <div className="row g-0 align-items-xl-center">
                <div className="col-lg-6 col-md-7 text-end order-md-2"><img className="d-inline-block" src="img/demo/business-consulting/hero-img.jpg" alt="Business people" width="945" /></div>
                <div className="col-lg-6 col-md-5 pt-5 pb-3 py-md-5">
                    <div className="mx-auto me-md-5 me-xl-7 ps-3 py-md-3 order-md-1" style={{ maxWidth: '516px' }}>
                    <h1 className="mb-grid-gutter pb-1 text-dark">Grow your business with Neighborhoodeals</h1>
                    <p className="callout d-block d-md-none d-lg-block fs-lg text-muted mb-grid-gutter">Manage the free listing for your business today.</p>
                    <div className="pt-2">
                        <div class="form-floating mb-3">
                            <input class="form-control" onChange={(e) => setbusinessname(e.target.value)} type="text" id="fl-text" placeholder="Your name" />
                            <label for="fl-text">Your Business name</label>
                        </div>
                        <button type="button" onClick={registerbusiness} class="btn btn-primary">Continue <i className="fas fa-arrow-right"></i></button>
                    </div>
                    </div>
                </div>
                </div>
            </section>
            <section className="bg-secondary bg-size-cover py-5 py-md-6" style={{ backgroundImage: 'url(img/demo/business-consulting/cta-bg.png)' }}>
                <div className="container py-3 py-md-0">
                <div className="row align-items-center">
                    <div className="col-lg-5 col-md-6 text-center text-md-start">
                    <h2 className="mb-4 pb-3 pb-md-0 mb-md-0">Get Contacted</h2>
                    </div>
                    <div className="col-lg-7 col-md-6">
                    <form className="row needs-validation" novalidate="">
                        <div className="col-sm-7 col-md-12 col-lg-7 mb-3 mb-lg-0">
                        <input className="form-control" type="email" placeholder="Your email" required="" />
                        <div className="invalid-feedback">Please provide a valid email address!</div>
                        </div>
                        <div className="col-sm-5 col-md-12 col-lg-5">
                        <button className="btn btn-primary d-block w-100" type="submit">Contact</button>
                        </div>
                    </form>
                    <div className="d-flex align-items-center justify-content-center justify-content-sm-start pt-4 pt-sm-2 pt-md-4"><i className="ai-phone fs-xl me-2"></i><span className="me-2">Or call us</span><a className="nav-link-style" href="tel:+15262200459">+ 1 526 220 0459</a></div>
                    </div>
                </div>
                </div>
            </section>
            <section className="container py-5 py-md-6 py-lg-7 my-3">
                <div className="row align-items-center">
                <div className="col-lg-4 col-md-5 text-center text-md-start mb-5 mb-md-0">
                    <h2 className="mb-3">Our Colutions</h2>
                    <p className="text-muted mb-4 pb-2">Find aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint doloremque.</p><a className="btn btn-primary" href="#">See all tools</a>
                </div>
                <div className="col-lg-8 col-md-7 bg-position-center bg-repeat-0" style={{ backgroundImage: 'url(img/demo/business-consulting/services/bg-shape.svg);' }}>
                    <div className="mx-auto" style={{ maxWidth: '610px' }}>
                    <div className="row align-items-center">
                        <div className="col-sm-6">
                        <div className="bg-light shadow-lg rounded-3 p-4 mb-grid-gutter text-center text-sm-start"><img className="d-inline-block mb-4 mt-2" src="img/demo/business-consulting/services/01.svg" alt="Icon" width="100" />
                            <h3 className="h5 mb-2">Solution 1</h3>
                            <p className="fs-sm">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries</p>
                        </div>
                        <div className="bg-light shadow-lg rounded-3 p-4 mb-grid-gutter text-center text-sm-start"><img className="d-inline-block mb-4 mt-2" src="img/demo/business-consulting/services/02.svg" alt="Icon" width="100" />
                            <h3 className="h5 mb-2">Solution 2</h3>
                            <p className="fs-sm">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries</p>
                        </div>
                        <div className="bg-light shadow-lg rounded-3 p-4 mb-grid-gutter text-center text-sm-start"><img className="d-inline-block mb-4 mt-2" src="img/demo/business-consulting/services/03.svg" alt="Icon" width="100" />
                            <h3 className="h5 mb-2">Solution 3</h3>
                            <p className="fs-sm">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries</p>
                        </div>
                        </div>
                        <div className="col-sm-6">
                        <div className="bg-light shadow-lg rounded-3 p-4 mb-grid-gutter text-center text-sm-start"><img className="d-inline-block mb-4 mt-2" src="img/demo/business-consulting/services/04.svg" alt="Icon" width="100" />
                            <h3 className="h5 mb-2">Solution 4</h3>
                            <p className="fs-sm">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries</p>
                        </div>
                        <div className="bg-light shadow-lg rounded-3 p-4 mb-grid-gutter text-center text-sm-start"><img className="d-inline-block mb-4 mt-2" src="img/demo/business-consulting/services/05.svg" alt="Icon" width="100" />
                            <h3 className="h5 mb-2">Solution 5</h3>
                            <p className="fs-sm">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries</p>
                        </div>
                        </div>
                    </div>
                    </div>
                </div>
                </div>
            </section>
            </main>
            <Footer />
        </>
    )
}

export default Home
