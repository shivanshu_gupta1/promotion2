import './App.css';
import { BrowserRouter as Router, Route } from "react-router-dom";

// Components
import Home from './components/Home';
import Registerbusiness from './components/Registerbusiness';
import Thankyou from './components/Thankyou';

function App() {
  return (
    <>
      <Router>
        <Route path="/" exact component={Home} />
        <Route path="/register" exact component={Registerbusiness} />
        <Route path="/thankyou" exact component={Thankyou} />
      </Router>
    </>
  );
}

export default App;
